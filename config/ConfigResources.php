<?php

namespace Contruder\Doctrine;

class ConfigResources
{
    /**
     * @return string
     */
    public static function getConfigFilePath()
    {
        return __DIR__ . "/Config.tyml";
    }
} 