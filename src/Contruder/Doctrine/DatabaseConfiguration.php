<?php

namespace Contruder\Doctrine;

interface DatabaseConfiguration
{
    /**
     * @return array The configuration as array
     */
    function toArray();
}
