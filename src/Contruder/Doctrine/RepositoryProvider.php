<?php

namespace Contruder\Doctrine;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Doctrine\ORM\EntityManager;
use Hediet\Types\ObjectType;

class RepositoryProvider implements ValueProvider
{   
    /**
     * @var WritableObjectRepository
     */
    private $writableRepo;
    
    /**
     * @param ObjectType $itemType (default attribute)
     * @param EntityManager $entityManager (default attribute)
     */
    public function __construct(ObjectType $itemType, EntityManager $entityManager)
    {
        $repo = $entityManager->getRepository($itemType->getName());
        $this->writableRepo = new WritableObjectRepositoryAdapter($repo, $entityManager);
    }
    
    /**
     * 
     * @param ServiceProvider $serviceProvider
     * @return WritableObjectRepository
     */
    public function provideValue(ServiceProvider $serviceProvider) 
    {
        return $this->writableRepo;
    }
}