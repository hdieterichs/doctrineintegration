<?php

namespace Contruder\Doctrine;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;

class EntityManagerProvider implements ValueProvider
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(Configuration $metadataConfiguration, 
            DatabaseConfiguration $databaseConfiguration,
            EventManager $eventManager)
    {
        $config = $databaseConfiguration->toArray();
        $this->entityManager = EntityManager::create($config, 
                $metadataConfiguration, $eventManager);
    }

    public function provideValue(ServiceProvider $serviceProvider)
    {
        return $this->entityManager;
    }
}


