<?php

namespace Contruder\Doctrine;

use \Nunzion\IO\Directory;

class MetadataPathProvider
{
    /**
     * @var Directory[]
     */
    private $paths;

    /**
     * @var MetadataPathProvider
     */
    private $next;

    /**
     *
     * @param Directory[] $paths (default attribute)
     * @param MetadataPathProvider $next
     */
    public function __construct(array $paths, MetadataPathProvider $next = null)
    {
        $this->paths = $paths;
        $this->next = $next;
    }

    /**
     * Returns all paths.
     * @return Directory[] paths
     */
    public function getPaths()
    {
        if ($this->next != null)
            return array_merge($this->next->getPaths(), $this->paths);
        else
            return $this->paths;
    }
}
