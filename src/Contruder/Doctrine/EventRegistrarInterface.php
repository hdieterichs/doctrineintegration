<?php

namespace Contruder\Doctrine;

use Doctrine\Common\EventManager;

interface EventRegistrarInterface 
{
    function registerEvents(EventManager $eventManager);
}
