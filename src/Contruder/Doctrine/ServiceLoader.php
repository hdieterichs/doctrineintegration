<?php

namespace Contruder\Doctrine;

use Contruder\ContruderHelper;
use Contruder\Php\Runtime\Services\Initializer;
use Contruder\Php\Runtime\Services\ServiceContainer;

class ServiceLoader implements Initializer
{
    /**
     * @return callable
     */
    public function initialize()
    {
        /* @var $serviceContainer ServiceContainer */
        $serviceContainer = ContruderHelper::loadObjectGraphFromFile(
            ConfigResources::getConfigFilePath()
        );

        return $serviceContainer->load();
    }
} 