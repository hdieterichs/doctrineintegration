<?php

namespace Contruder\Doctrine;

use \Nunzion\Expect;

class MySqlDatabaseConfiguration implements DatabaseConfiguration
{
    /**
     * @var array
     */
    private $configuration;

    /**
     * 
     * @param string $user
     * @param string $database
     * @param string $host
     * @param string $password
     * @param int $port
     * @param string $charset
     * @param string $sslmode
     */
    public function __construct($user, $database, $host = null, $password = null, $port = null,
                                $charset = null, $sslmode = null)
    {
        Expect::that($user)->isString();
        Expect::that($database)->isString();

        $this->configuration = array(
            "driver" => "pdo_mysql",
            "user" => $user,
            "dbname" => $database
        );

        if ($host != null)
        {
            Expect::that($host)->isString();
            $this->configuration["host"] = $host;
        }
        if ($password != null)
        {
            Expect::that($password)->isString();
            $this->configuration["password"] = $password;
        }
        if ($port != null)
        {
            Expect::that($port)->isInt();
            $this->configuration["port"] = $port;
        }
        if ($charset != null)
        {
            Expect::that($charset)->isString();
            $this->configuration["charset"] = $charset;
        }
        if ($sslmode != null)
        {
            Expect::that($sslmode)->isString();
            $this->configuration["sslmode"] = $sslmode;
        }
    }

    /**
     * {@inheritdoc}
     * @return array The configuration as array
     */
    public function toArray()
    {
        return $this->configuration;
    }

}
