<?php

namespace Contruder\Doctrine;

use \Contruder\Common\ServiceProvider;
use \Contruder\Php\Construction\ValueProvider;
use \Doctrine\ORM\Configuration;
use \Doctrine\ORM\Tools\Setup;

class YamlMetadataConfigurationProvider implements ValueProvider
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * Creates an XmlMetadataConfigurationProvider.
     * @param MetadataPathProvider $pathProvider
     * @param boolean $isDevMode
     * @param string $proxyDir
     */
    public function __construct(MetadataPathProvider $pathProvider, $isDevMode = false, $proxyDir = null)
    {
        // TODO? Cache
        $this->configuration = Setup::createYAMLMetadataConfiguration($pathProvider->getPaths(), $isDevMode, $proxyDir);
    }

    public function provideValue(ServiceProvider $serviceProvider)
    {
        return $this->configuration;
    }
}


