<?php

namespace Contruder\Doctrine;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Doctrine\Common\EventManager;

class EventManagerProvider implements ValueProvider
{

    /**
     * @var EventRegistrarInterface
     */
    private $eventRegistrar;

    public function __construct(EventRegistrarInterface $eventRegistrar)
    {
        $this->eventRegistrar = $eventRegistrar;
    }
    
    /**
     * @param ServiceProvider $serviceProvider
     * @return EventManager
     */
    public function provideValue(ServiceProvider $serviceProvider) 
    {
        $result = new EventManager();
        $this->eventRegistrar->registerEvents($result);
        return $result;
    }
}
