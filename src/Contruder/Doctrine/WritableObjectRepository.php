<?php

namespace Contruder\Doctrine;

interface WritableObjectRepository extends \Doctrine\Common\Persistence\ObjectRepository
{   
    function flush();
    
    function persist($entity);
    
    function save($entity);
    
    function remove($entity);
}