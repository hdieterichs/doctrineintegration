<?php

namespace Contruder\Doctrine;

use Doctrine\Common\EventManager;

class EventRegistrar implements EventRegistrarInterface 
{

    /**
     * @var EventRegistrarInterface
     */
    private $next;
    private $listener;
    private $event;

    /**
     * 
     * @param string $event (default attribute)
     * @param object $listener (default attribute)
     * @param EventRegistrarInterface $next
     */
    public function __construct($event, $listener, EventRegistrarInterface $next = null)
    {
        $this->event = $event;
        $this->listener = $listener;
        $this->next = $next;
    }
    
    public function registerEvents(EventManager $eventManager)
    {
        $eventManager->addEventListener($this->event, $this->listener);
    }
}
