<?php

namespace Contruder\Doctrine;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Setup;
use Nunzion\IO\Directory;

class AnnotationMetadataConfigurationProvider implements ValueProvider
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * 
     * @param MetadataPathProvider $pathProvider
     * @param boolean $isDevMode
     * @param Directory $proxyDir
     */
    public function __construct(MetadataPathProvider $pathProvider, $isDevMode = false, Directory $proxyDir = null)
    {
        // TODO Cache
        
        $paths = array();
        foreach ($pathProvider->getPaths() as $dir)
            $paths[] = $dir->getPath();
        
        $proxyDirPath = null;
        if ($proxyDir !== null)
            $proxyDirPath = $proxyDir->getPath();
        
        $this->configuration = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir);
    }

    public function provideValue(ServiceProvider $serviceProvider)
    {
        return $this->configuration;
    }
}


